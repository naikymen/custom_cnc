# Custom CNC machine models

Licencing details here: [HARDWARE_LICENCE.txt](HARDWARE_LICENCE.txt) (CERN Open Hardware Licence Version 2 - Strongly Reciprocal).

Features:

* 3 axis cartesian machine.
* Double Y axis.
* Double Z axis.
* Tool mounts for:
  * NEJE A40640
  * Dremel 3000 (requires extra aluminium plate).

Pending:

* End-stop mounts.
* Assembly guide.
* Bill of materials.
* Duplicate X axis rods (increase frame stiffness).

Pictures:

![reference machine](images/prototype.png)

# Project directory structure

* `external_parts`: Dremel 3000 mounting nut.
* `freecad_exports`: exported STL models for the single Z-motor machine.
* `freecad_exports_doble_z`: exported STL models for the double Z-motor machine, and PrusaSlicer project.
* `gcode`: rendered gcode for parts (untracked by git).
* `images`: images for this readme.
